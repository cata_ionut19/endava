from flask import Flask,request
from math_module import Math
from db_module import Database

app = Flask(__name__)
objm=Math()
objd=Database()

@app.route("/pow",methods=['POST'])
def api_pow():
    try:
        json=request.json
        base=json["base"]
        power=json["power"]
        result=objm.Pow(base,power)
        response=result["response"]
        objd.insert_req("pow",f"Baza:{base},Exponent:{power}",f"Rezultat:{response}")
        return result
    except Exception as e:
        objd.insert_error(e)
        return {"response":e}
    

@app.route("/fibbonaci",methods=['POST'])
def api_fibbonaci():
    try:
        json=request.json
        value=json["value"]
        result=objm.Fibbonaci(value)
        response=result["response"]
        objd.insert_req("fibbonaci",f"Valoare:{value}",f"Rezultat:{response}")
        return result
    except Exception as e:
        objd.insert_error(str(e))
        return {"response":e}
   

@app.route("/factorial",methods=['POST'])
def api_factorial():
    try:
        json=request.json
        value=json["value"]
        result=objm.Factorial(value)
        response=result["response"]
        objd.insert_req("factorial",f"Valoare:{value}",f"Rezultat:{response}")
        return result
    except Exception as e:
        objd.insert_error(e)
        return {"response":e}

if __name__ == '__main__':
    app.run(host="0.0.0.0",port=5000)