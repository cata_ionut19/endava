class Math:

    def __check_number__(self,n):
        if isinstance(n, str):
            if n.isnumeric():
                return int(n)
            else:
                return False
        return n

    def Pow(self,n,m):
        n=self.__check_number__(n)
        m=self.__check_number__(m)
        if m!=False and n!=False:
            return {"response":pow(n,m)}
        return {"response":"Format invalid!"}
        
    
    def Fibbonaci(self,n):
        n1, n2 = 0, 1
        count = 0

        n=self.__check_number__(n)
        if n!=False:
            if n < 0:
                return {"response":"Negative number"}
            elif n == 1:
                return {"response":1}
            elif n == 0:
                return {"response":0}
            else:
                while count < n:
                    nth = n1 + n2
                    n1 = n2
                    n2 = nth
                    count += 1
                return {"response":n1}
        return {"response":"Format invalid!"}

    def Factorial(self,n):
        factorial = 1
        n=self.__check_number__(n)
        if n!=False:
            if n < 0:
                return {"response":"Negative number"}
            elif n == 0:
                return {"response":1}
            else:
                for i in range (1, n+1):
                    factorial = factorial *i
                return {"response":factorial}
        return {"response":"Negative number"}



