import sqlite3

class Database():
    def __init__(self):
       
        self.con=sqlite3.connect("database.db",check_same_thread=False)
        self.cur = self.con.cursor()
        self.cur.execute("CREATE TABLE if not exists requests(name, value, result)")
        self.cur.execute("CREATE TABLE if not exists errors(value)")
       

    def insert_req(self,name,value,result):
        try:
            params = (name,value,result)
            self.cur.execute('INSERT INTO requests VALUES(?,?,?)',params)
            self.con.commit()
            for row in self.cur.execute("SELECT * from requests"):
                print(row)
        except Exception as e:
            print(e)
    
    def insert_error(self,value):
        try:
            params = (value)
            self.cur.execute('INSERT INTO errors VALUES(?)',params)
            self.con.commit()
            for row in self.cur.execute("SELECT * from errors"):
                    print(row)
        except Exception as e:
            print(e)


    


# db=Database()
# db.insert_req('gdgfd','fasfas','fass')